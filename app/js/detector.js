window.URL = window.URL || window.webkitURL;

var camvideo = document.getElementById('monitor');
navigator.getUserMedia({ video: true }, goStream, noStream);

function goStream(stream) {
	camvideo.src = window.URL.createObjectURL(stream);
	camvideo.onerror = function(e) {
		stream.stop();
	};
	stream.onended = noStream;
}

function noStream(e) {
	var msg = 'No camera available.';
	if (e.code == 1) {
		msg = 'User denied access to use camera.';
	}
	console.log(msg);
}

// standard global variables
var container, scene, camera, renderer, cube;

// assign global variables to HTML elements
var video = document.getElementById('monitor');
var videoCanvas = document.getElementById('videoCanvas');
var videoContext = videoCanvas.getContext('2d');

var layer2Canvas = document.getElementById('layer2');
var layer2Context = layer2Canvas.getContext('2d');

var blendCanvas = document.getElementById('blendCanvas');
var blendContext = blendCanvas.getContext('2d');

var buttons;
var lastImageData;

init();
animate();

// FUNCTIONS
function init() {
	// VIDEO SET UP
	// these changes are permanent
	videoContext.translate(300, 0);
	videoContext.scale(-1, 1);

	buttons = [];

	var button1 = new Image();
	button1.src = 'img/SquareRed.png';
	var buttonData1 = { name: 'red', image: button1, x: 150, y: 10, w: 32, h: 32 };
	buttons.push(buttonData1);

	var button2 = new Image();
	button2.src = 'img/SquareGreen.png';
	var buttonData2 = { name: 'green', image: button2, x: 200, y: 10, w: 32, h: 32 };
	buttons.push(buttonData2);

	var button3 = new Image();
	button3.src = 'img/SquareBlue.png';
	var buttonData3 = { name: 'blue', image: button3, x: 250, y: 10, w: 32, h: 32 };
	buttons.push(buttonData3);
}

function animate() {
	requestAnimationFrame(animate);
	render();
	update();
}

function update() {
	blend();
	checkAreas();
}

function render() {
	if (video.readyState === video.HAVE_ENOUGH_DATA) {
		// mirror video
		videoContext.drawImage(video, 0, 0, videoCanvas.width, videoCanvas.height);
		for (var i = 0; i < buttons.length; i++) {
			layer2Context.drawImage(buttons[i].image, buttons[i].x, buttons[i].y, buttons[i].w, buttons[i].h);
		}
	}
}

function blend() {
	var width = videoCanvas.width;
	var height = videoCanvas.height;
	// get current webcam image data
	var sourceData = videoContext.getImageData(0, 0, width, height);
	// create an image if the previous image doesn�t exist
	if (!lastImageData) lastImageData = videoContext.getImageData(0, 0, width, height);
	// create a ImageData instance to receive the blended result
	var blendedData = videoContext.createImageData(width, height);
	// blend the 2 images
	differenceAccuracy(blendedData.data, sourceData.data, lastImageData.data);
	// draw the result in a canvas
	blendContext.putImageData(blendedData, 0, 0);
	// store the current webcam image
	lastImageData = sourceData;
}
function differenceAccuracy(target, data1, data2) {
	if (data1.length != data2.length) return null;
	var i = 0;
	while (i < data1.length * 0.25) {
		var average1 = (data1[4 * i] + data1[4 * i + 1] + data1[4 * i + 2]) / 3;
		var average2 = (data2[4 * i] + data2[4 * i + 1] + data2[4 * i + 2]) / 3;
		var diff = threshold(fastAbs(average1 - average2));
		target[4 * i] = diff;
		target[4 * i + 1] = diff;
		target[4 * i + 2] = diff;
		target[4 * i + 3] = 0xff;
		++i;
	}
}
function fastAbs(value) {
	return (value ^ (value >> 31)) - (value >> 31);
}
function threshold(value) {
	return value > 0x15 ? 0xff : 0;
}

// check if white region from blend overlaps area of interest (e.g. triggers)
function checkAreas() {
	for (var b = 0; b < buttons.length; b++) {
		// get the pixels in a note area from the blended image
		var blendedData = blendContext.getImageData(buttons[b].x, buttons[b].y, buttons[b].w, buttons[b].h);
		// calculate the average lightness of the blended data
		var sum = 0;
		var countPixels = blendedData.data.length / 4;
		for (var i = 0; i < countPixels; ++i) {
			sum += blendedData.data[i * 4] + blendedData.data[i * 4 + 1] + blendedData.data[i * 4 + 2];
		}
		// calculate an average between of the color values of the note area [0-255]
		var average = Math.round(sum / (4 * countPixels));
		if (average > 50) {
			// more than 20% movement detected
			console.log(buttons[b].name); // do stuff
			$('button').css('background-color', buttons[b].name);
		}
	}
}
